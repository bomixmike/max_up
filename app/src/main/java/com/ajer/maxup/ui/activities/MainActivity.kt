package com.ajer.maxup.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ajer.maxup.R
import com.ajer.maxup.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}